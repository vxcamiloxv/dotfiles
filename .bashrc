# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]; then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

############
# MOTD
############
echo "$(tput setaf 2)
 |
 |  GNU en `hostname`
 |  `date +"%A, %e %B %Y, %r"`
 |  Linux-Libre `uname -rm`
 |  $(tput setaf 1)
$(tput sgr0)"

if hash cowsay 2>/dev/null && hash fortune 2>/dev/null; then
    export DAIKICHI_FORTUNE_PATH="$HOME/.fortunes/fortunes/es:$DAIKICHI_FORTUNE_PATH"
    fortune ciencia vida libertad hackers liberacion anarchism | cowsay -f "$HOME/.cowsay/small.cow"
fi

#######################
# Alias definitions
#######################

# Extends additional aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

########################
# Init management
########################

# SSH management
if hash keychain 2>/dev/null; then
    eval $(keychain  --eval --noask --quiet)
else
    SSH_PID="$XDG_RUNTIME_DIR/ssh-agent.pid"
    if ! pgrep -u "$USER" ssh-agent > /dev/null; then
        ssh-agent > "$SSH_PID"
    fi
    if [[ ! "$SSH_AUTH_SOCK" ]] && [[ -f "$SSH_PID" ]]; then
        eval "$(<"$SSH_PID")" > /dev/null
    fi
fi

# Start GNU Screen
#[[ -z "$STY" ]] && screen
#[[ $TERM != "screen" ]] && exec screen -q

########################
# Shell Variables
########################
export EDITOR="emacsclient"
#export EDITOR="$(if [[ -n $DISPLAY ]]; then echo 'em'; else echo 'emc'; fi)"
#export GIT_EDITOR="$(if [[ -n $DISPLAY ]]; then echo 'git-editor'; else echo 'emc'; fi)"
export RANGER_LOAD_DEFAULT_RC=FALSE

#Custom PATH
PATH="$HOME/.scripts/:$HOME/.local/bin/:$PATH"

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

#######################
# Set shell behavior
#######################

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Disable completion when the input buffer is empty.  i.e. Hitting tab
# and waiting a long time for bash to expand all of $PATH.
shopt -s no_empty_cmd_completion

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

#############################
# Distribution configuration
#############################

# On Debian set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
    chroot_prompt="${debian_chroot:+($debian_chroot)}"
fi

#######################
# Colors support
#######################
# Force Enable urxvt 256 colors support
export TERM=xterm-256color

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    [aEkx]term*|rxvt*|gnome*|konsole*|screen|cons25|*color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=no
    fi
fi

#######################
# Prompt setup
#######################
# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]; then
    env_prompt=' [env]'
else
    env_prompt=''
fi

if [ "$color_prompt" = yes ]; then
    # default: PS1='\u@\h \w\$ '
    PS1='${chroot_prompt}\[\e[0;0m\]┌─ \u\[\e[33;1m\]@\h \[\033[01;34m\]\W${env_prompt}\[\033[00m\] \[\e[0;1m\]\n└──┤|▶ \[\e[0m\]'
else
    PS1='${chroot_prompt}\u@\h \W${env_prompt}\$ '
fi
unset color_prompt force_color_prompt

# # Change the window title of X terminals
case ${TERM} in
    [aEkx]term*|rxvt*|gnome*|konsole*|interix)
        PS1="\[\e]0;${chroot_prompt}\u@\h:\w\a\]$PS1"
        ;;
    screen*)
        PS1="\[\033k${chroot_prompt}\u@\h:\w\033\\\]$PS1"
        ;;
    *)
        ;;
esac

# GuixSD needs this? it still requesting it
# Arrange so that ~/.config/guix/current comes first.
for profile in "$HOME/.guix-profile" "$HOME/.config/guix/current"
do
    if [ -f "$profile/etc/profile" ]
    then
        # Load the user profile's settings.
        GUIX_PROFILE="$profile" ; \
            . "$profile/etc/profile"
    else
        # At least define this one so that basic things just work
        # when the user installs their first package.
        export PATH="$profile/bin:$PATH"
    fi
done
unset profile

##########################
# Load additional path
# definitions
##########################

#Node.js Tools
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
    *":$PNPM_HOME:"*) ;;
    *) export PATH="$PNPM_HOME:$PATH" ;;
esac

#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

###########################
# Completion configuration
###########################

# Enable programmable completion features, For non GuixSD systems but maybe not
# need due /etc/bashrc is already source.
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    source /etc/bash_completion
fi

###-begin-npm-completion-###
#
# npm command completion script
#
# Installation: npm completion >> ~/.bashrc  (or ~/.zshrc)
# Or, maybe: npm completion > /usr/local/etc/bash_completion.d/npm
#

if type complete &>/dev/null; then
    _npm_completion () {
	local words cword
	if type _get_comp_words_by_ref &>/dev/null; then
	    _get_comp_words_by_ref -n = -n @ -w words -i cword
	else
	    cword="$COMP_CWORD"
	    words=("${COMP_WORDS[@]}")
	fi

	local si="$IFS"
	IFS=$'\n' COMPREPLY=($(COMP_CWORD="$cword" \
					 COMP_LINE="$COMP_LINE" \
					 COMP_POINT="$COMP_POINT" \
					 npm completion -- "${words[@]}" \
					 2>/dev/null)) || return $?
	IFS="$si"
    }
    complete -o default -F _npm_completion npm
elif type compdef &>/dev/null; then
    _npm_completion() {
	local si=$IFS
	compadd -- $(COMP_CWORD=$((CURRENT-1)) \
			       COMP_LINE=$BUFFER \
			       COMP_POINT=0 \
			       npm completion -- "${words[@]}" \
			       2>/dev/null)
	IFS=$si
    }
    compdef _npm_completion npm
elif type compctl &>/dev/null; then
    _npm_completion () {
	local cword line point words si
	read -Ac words
	read -cn cword
	let cword-=1
	read -l line
	read -ln point
	si="$IFS"
	IFS=$'\n' reply=($(COMP_CWORD="$cword" \
				     COMP_LINE="$line" \
				     COMP_POINT="$point" \
				     npm completion -- "${words[@]}" \
				     2>/dev/null)) || return $?
	IFS="$si"
    }
    compctl -K _npm_completion npm
fi
###-end-npm-completion-###

###########################
# Isolate project env
###########################
eval "$(direnv hook bash)"
