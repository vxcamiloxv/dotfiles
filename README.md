# Distopico Dotfiles

My `.files` use Guix [home][guix-home-blog], it's can be used in any Gnu/Linux
distribution, this configuration is primary for Guix OS, Parabola and sometimes
Debian.


[guix-home-blog]: https://guix.gnu.org/en/blog/2022/keeping-ones-home-tidy/
