# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi

os_name=$(echo | grep -E '^ID=' /etc/os-release | sed s/ID=//)

# Missing Guix env setup?
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/.guix-profile/lib/pkgconfig
export XDG_DATA_DIRS=$XDG_DATA_DIRS:$HOME/.nix-profile/share
export FONTCONFIG_PATH=/run/current-system/profile/etc/fonts:$HOME/.guix-profile/etc/fonts${FONTCONFIG_PATH:+:}$FONTCONFIG_PATH
export DICPATH=$HOME/.local/share/hunspell${DICPATH:+:}$DICPATH
export PATH=$PATH:/usr/local/bin:$HOME/.local/bin:$HOME/.scripts

if [[ "$os_name" = "guix" ]]; then
    # Icu timezone bug https://debbugs.gnu.org/cgi/bugreport.cgi?bug=68411
    export TZ=$(cat /etc/timezone)

    ## export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
    # In theory this is done by /etc/profile
    ## export GUIX_PROFILE="$HOME/.guix-profile"
    ##. "$GUIX_PROFILE/etc/profile"

    # Enbling Nix to use packages not availble in Guix
    # such as "rustup" or "appimage-run"
    source /run/current-system/profile/etc/profile.d/nix.sh
fi

# Makes Guix happy
for locale in "$HOME/.guix-profile/lib/locale" "/run/current-system/locale"
do
    if [ -f "$locale" ]
    then
	export GUIX_LOCPATH="$locale${GUIX_LOCPATH:+:$GUIX_LOCPATH}"
    fi
done
unset locale

# Uniform lock and feel gtk/qt
# other option: QT_STYLE_OVERRIDE=breeze, kvantum or gtk2
export QT_QPA_PLATFORMTHEME=qt5ct

if [[ $XDG_SESSION_DESKTOP = "awesome" ]]; then
    # Just becase I'm using components from there
    export XDG_CURRENT_DESKTOP=xfce
    export XDG_MENU_PREFIX=xfce-

    # Run on start-up
    if [[ "$os_name" = "guix" ]]; then
	/run/current-system/profile/libexec/polkit-mate-authentication-agent-1 &
	/run/current-system/profile/bin/pulseaudio --start --log-target=syslog &
    fi
fi

# Should restore alsa state? alsactl --file ~/.config/asound.state restore|store

# X11 wants to blank the screen out of the box. To prevent, try putting
# xset dpms 0 0 0 && xset s noblank  && xset s off
