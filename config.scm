;; To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (ice-9 rdelim)
	     (ice-9 format)
	     (guix gexp)
	     (guix channels)
	     (gnu home)
	     (gnu packages)
	     (gnu services)
	     (gnu home services)
	     (gnu home services guix)
	     (gnu home services shells)
	     (gnu home services music))

(define (config-file-to-string file)
  (catch 'system-error
    (lambda ()
      (read-string
       (open-input-file
	(string-append (dirname (current-filename)) "/" file))))
    (lambda (key _subr message args . _)
      (throw key (apply format
			(current-error-port)
			"Error reading file - ~A: ~S~%" args) file))))

(home-environment
 (packages (specifications->packages
	    (list
	     "alacritty"
	     "audacity"
	     "blender"
	     "blueman"
	     "breeze"
	     "breeze-gtk"
	     "breeze-icons"
	     "clamav"
	     "clipmenu"
	     "cowsay"
	     "daikichi"
	     "desktop-file-utils"
	     "direnv"
	     "emacs"
	     "font-hack"
	     "fortunes-jkirchartz"
	     "gimp"
	     "gnome-keyring"
	     "h-client"
	     "handbrake"
	     "imagemagick"
	     "inkscape"
	     "inxi"
	     "jami"
	     "kdeconnect"
	     "kdenlive"
	     "kdiskmark"
	     "keychain"
	     "kid3"
	     "libnotify"
	     "libreoffice"
	     "maim"
	     "meld"
	     "mkvtoolnix:gui"
	     "mu"
	     "mupdf"
	     "nextcloud-client"
	     "nnn"
	     "nsxiv"
	     "offlineimap3"
	     "pass-otp"
	     "password-store"
	     "pavucontrol"
	     "pdfarranger"
	     "picom"
	     "python-wrapper"
	     "qemu"
	     "qutebrowser"
	     "rxvt-unicode"
	     "supertuxkart"
	     "tabbed"
	     ;;"telegram-desktop"
	     "thunar-archive-plugin"
	     "transmission"
	     "transmission:gui"
	     "transmission-remote-gtk"
	     "virt-manager"
	     "vlc"
	     "vorbis-tools"
	     "yt-dlp"
	     "zathura"
	     "zathura-cb"
	     "zathura-djvu"
	     "zathura-pdf-mupdf"
	     "zathura-ps"
	     ;; Browsers
	     "falkon"
	     "icecat"
	     "librewolf"
	     "nyxt"
	     ;; Video
	     "simplescreenrecorder"
	     "peek"
	     ;; Desktop utils
	     "redshift"
	     "rofi"
	     ;; Command-line tools
	     "calc"
	     "lshw"
	     "htop"
	     "socat"
	     "zbar"
	     ;; Command-line utils
	     "acpica"
	     "xsel"
	     "unzip"
	     "zip"
	     "pigz"
	     "pv"
	     ;; Dev tools
	     "global"
	     "python-pygments"
	     "rust"
	     "rust:cargo"
	     "rust:tools")))

 (services
  (list
   (service home-bash-service-type
	    (home-bash-configuration
	     (aliases '(;; Modified commands
			("l" . "ls -CF")
			("la" . "ls -A")
			("lla" . "ls -alF")

			;; Utility commands
			("mpc" . "ncmpcpp")
			("emc" . "em -nw")
			;;("acortar" . "curl -s \"http://is.gd/create.php?format=simple&url=$(xsel -po)\" | xsel -pi")
			("apagar" . "sudo shutdown -h 14:20")
			("bajar" . "plowdown")
			("subir" . "plowup")
			("search-word" . "find . -type f -print0 | xargs -0 grep -l ")
			("s" . "screen")
			("screen" . "screen -aAxRl")
			("jpg-optimized" . "find . -name *.jpg -exec jpegoptim --max=80 -t '\\'''\\'' {} \\;")
			("prime-disable" . "export DRI_PRIME=0")
			("prime-enable" . "export DRI_PRIME=1")
			("mu4e" . "em -c -n --eval '\\''(mu4e)'\\''")
			("webcam" . "webcam_mods bg-blur")

			;; Custom ~/.scripts commands
			("pass" . "$HOME/.scripts/pass-utils/passp")
			("pomf" . "$HOME/.scripts/pomf.py")
			("pomf-w" . "$HOME/.scripts/scrotpomf.sh")
			("capas2png" . "$HOME/.scripts/Inkscape/layers2pngs.py")
			("davpush" . "$HOME/.scripts/davpush.pl")
			("tvgnu" . "$HOME/.scripts/TVenGNU.sh")
			("haste" . "HASTE_SERVER=http://vte.distopico.info haste")
			("hastebin" . "$HOME/.scripts/haste.sh")

			;; Alias directories
			("home" . "cd $HOME/")
			("descargas" . "cd $HOME/Downloads")
			("escritorio" . "cd $HOME/Desktop")
			("musica" . "cd $HOME/Music")
			("imagenes" . "cd $HOME/Pictures")
			("publico" . "cd $HOME/Public")
			("videos" . "cd $HOME/Videos")

			;; Dist Parabola
			("update-grub" . "sudo grub-mkconfig -o /boot/grub/grub.cfg")

			;; Dist Debian Based
			("repo" . "sudo add-apt-repository ")
			("build-dep" . "sudo apt-get build-dep ")
			("dist-upgrade" . "sudo apt-get dist-upgrade")
			("search-app" . "sudo aptitude search")
			("update-full" . "sudo aptitude full-upgrade -y && sudo aptitude clean")

			;; Dist Guix
			("guix-reconfigure" . "sudo guix system reconfigure /etc/config.scm; sudo update-boot")
			("guix-home" . "guix home reconfigure ~/src/guix-config/config.scm")))
	     (bashrc `(,(local-file ".bashrc" "bashrc")))
	     (bash-profile `(,(local-file ".bash_profile" "bash_profile")))))

   (service home-beets-service-type
	    (home-beets-configuration
	     (directory "/net/shared/Music/camilo")
	     (extra-options `(,(config-file-to-string "beets/config.yaml")))))

   (simple-service 'base-configuration
		   home-files-service-type
		   `((".Xresources" ,(local-file ".Xresources" "Xresources"))
		     (".Xdefaults" ,(local-file ".Xdefaults" "Xdefaults"))))

   (simple-service 'cowsay-configuration
		   home-files-service-type
		   `((".cowsay/small.cow" ,(local-file "cowsay/small.cow"))))

   (simple-service 'variant-packages-service
		   home-channels-service-type
		   (cons
		    (channel
		     (name 'rustup)
		     (url "https://github.com/declantsien/guix-rustup")
		     (introduction
		      (make-channel-introduction
		       "325d3e2859d482c16da21eb07f2c6ff9c6c72a80"
		       (openpgp-fingerprint
			"F695 F39E C625 E081 33B5  759F 0FC6 8703 75EF E2F5"))))
		    %default-channels)))))
